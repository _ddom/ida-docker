import sys
import os
import idc
import logging
import ida_pro

STDOUT_NOT_SET = 2
STDERR_NOT_SET = 3

def finish():
    sys.stdout.flush()
    sys.stderr.flush()
    sys.exit()

if "IDASCRIPT_STDOUT" not in os.environ:
    with open("idascript_error.log") as fd:
        fd.write("IDASCRIPT_STDOUT not set!")
    sys.exit(STDOUT_NOT_SET)

if "IDASCRIPT_STDERR" not in os.environ:
    with open("idascript_error.log") as fd:
        fd.write("IDASCRIPT_STDERR not set!")
    sys.exit(STDERR_NOT_SET)

sys.argv = idc.ARGV
sys.exit = lambda x=0: ida_pro.qexit(x)

sys.stdout = open(os.environ["IDASCRIPT_STDOUT"], "w")
sys.stderr = open(os.environ["IDASCRIPT_STDERR"], "w")


logging.basicConfig(format='%(asctime)s [idascript] %(levelname)s: %(message)s')
