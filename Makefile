D=docker

.PHONY=all clean idapro75-ubuntu-latest idapro75-ios-ubuntu-latest

all: idapro75-ubuntu-latest idapro75-ios-ubuntu-latest

idapro75-ubuntu-latest: idapro75-ubuntu-latest.dockerfile
	@$(D) build \
			-t idapro75:ubuntu-latest \
			--build-arg IDA_PASSWORD=$(IDA_PASSWORD) \
			--build-arg IDA_LICENSE_SERVER=$(IDA_LICENSE_SERVER) \
			-f idapro75-ubuntu-latest.dockerfile .

unicorn-1.0.1.tar.gz:
		@wget https://github.com/unicorn-engine/unicorn/archive/1.0.1.tar.gz -O unicorn-1.0.1.tar.gz

idapro75-ios-ubuntu-latest: unicorn-1.0.1.tar.gz idapro75-ios-ubuntu-latest.dockerfile
	@$(D) build \
			-t idapro75-ios:ubuntu-latest \
			-f idapro75-ios-ubuntu-latest.dockerfile .


clean:
	rm unicorn-1.0.1.tar.gz
