import idascript
import ida_auto

try:
    print("Running autoanalysis on %s" % idaapi.get_input_file_path())
    ida_auto.auto_wait()
    print("Done. Closing IDA.")
    idascript.finish()
except Exception as e:
    print("Something went wrong:", e)

