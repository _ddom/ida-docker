FROM idapro75:ubuntu-latest
MAINTAINER daniel.dominguez@imdea.org

USER root
RUN apt-get -y update && \
    apt-get -y install --fix-missing \
      apt-utils \
      libcmocka-dev \
      python3-pip \
      python3-setuptools \
      python3.8-distutils && \
    apt-get -qy clean autoremove && \
    rm -rf /var/lib/apt/lists/*
USER lovelace
# TODO: Change ARM_REGTRACK_MAX_XREFS in ida.cfg

COPY submodules/flare-emu/flare_emu.py $IDA_HOME/python/3/flare_emu.py
COPY submodules/flare-emu/flare_emu_hooks.py $IDA_HOME/python/3/flare_emu_hooks.py
COPY submodules/flare-emu/flare_emu_ida.py $IDA_HOME/python/3/flare_emu_ida.py
ADD submodules/flare-ida/python/flare $IDA_HOME/python/3/flare
ADD unicorn-1.0.1.tar.gz .

USER root
RUN chown -R lovelace:lovelace unicorn-1.0.1
USER lovelace

WORKDIR unicorn-1.0.1
RUN ./make.sh
WORKDIR bindings/python
#RUN pip3 install --upgrade setuptools distribute
USER root
#RUN apt-get update && \
#    apt-get install --reinstall \
#      python3-pkg-resources && \
#    apt-get -qy clean autoremove && \
#    rm -rf /var/lib/apt/lists/*
RUN python3 setup.py install
USER lovelace
#RUN ./sample_all.sh

WORKDIR $HOME
