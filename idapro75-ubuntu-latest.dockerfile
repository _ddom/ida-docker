FROM ubuntu:latest
MAINTAINER daniel.dominguez@imdea.org

ENV DEBIAN_FRONTEND noninteractive
ENV DISPLAY :0
ENV QT_X11_NO_MITSHM 1

# Not all of this packages are needed, but figuring out which ones are takes a lot of time.
RUN apt-get -y update && \
    apt-get -y install --fix-missing \
      lib32gcc1 \
      libfontconfig \
      libfreetype6 \
      libglib2.0-0 \
      libsm6 \
      libssl-dev \
      libstdc++6 \
      libxext6 \
      libxrender1 \
      lsb-core \
      python-dev \
      libglu1-mesa \
      libgl1-mesa-dev \
      libxi6 \
      qt5-default \
      sudo && \
    apt-get -qy clean autoremove && \
    rm -rf /var/lib/apt/lists/*

# Create a normal user for IDA
RUN useradd -p $(openssl passwd -1 ada) -d /home/lovelace -m -U -G sudo lovelace
RUN chown lovelace:lovelace /home/lovelace

USER lovelace
ENV HOME /home/lovelace
ENV IDA_HOME /home/lovelace/idapro-7.5
WORKDIR /home/lovelace

ADD ida-pro ida-pro-installer
USER root
RUN chown -R lovelace:lovelace ida-pro-installer
USER lovelace

ARG IDA_PASSWORD
ARG IDA_LICENSE_SERVER

RUN echo "HEXRAYS_LICENSE_FILE=@"$IDA_LICENSE_SERVER > ~/.flexlmrc

RUN ida-pro-installer/installer.run \
      --mode unattended \
      --prefix $IDA_HOME \
      --installpassword $IDA_PASSWORD

RUN touch $IDA_HOME/license.displayed
ENV PATH=$PATH:$IDA_HOME
RUN idapyswitch --auto-apply
RUN rm -rf ida-pro-installer

COPY idascript $IDA_HOME/idascript
COPY idascript.py $IDA_HOME/python/3/idascript.py
ADD scripts scripts


