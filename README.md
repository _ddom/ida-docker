# IDA in Docker

IDA 7.5 running inside docker containers

## Images

The user and password for all images is `lovelace` and `ada`. This user has sudo access

* `idapro75:ubuntu-latest`: A basic installation of IDA Pro based on
  `ubuntu:latest`
* `idapro75-ios:ubuntu-latest`: IDA Pro plus iOS goodies. Based on
  `idapro74:ubuntu-latest`.

## Building

* Extract your copy of IDA pro in `ida-pro`. Rename the installer script to
`installer.run` and the key to `ida.key`.
* Run `IDA_PASSWORD=<password> IDA_LICENSE_SERVER=<domain> make all` to build all the images

## `idascript`

The base images come bundled with `idascript`, a bash script that setups IDA for
scripting. Some scripts that use `idascript` can be found in the `scripts`
folder.

## Examples

Run the auto analysis on an empty database and then close:

```
docker run idapro75:ubuntu-latest idascript -t --64 scripts/genidb.py
```
